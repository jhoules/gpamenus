-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:2:1
	xmlns:android
		ADDED from AndroidManifest.xml:2:11
	package
		ADDED from AndroidManifest.xml:3:7
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionName
		ADDED from AndroidManifest.xml:4:7
	android:versionCode
		ADDED from AndroidManifest.xml:4:33
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
uses-sdk
ADDED from AndroidManifest.xml:5:5
	android:minSdkVersion
		ADDED from AndroidManifest.xml:5:15
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
application
ADDED from AndroidManifest.xml:7:5
	android:label
		ADDED from AndroidManifest.xml:7:47
	android:debuggable
		ADDED from AndroidManifest.xml:8:6
	android:icon
		ADDED from AndroidManifest.xml:7:18
activity#joanne.cs250.CourseList
ADDED from AndroidManifest.xml:9:9
	android:label
		ADDED from AndroidManifest.xml:10:19
	android:name
		ADDED from AndroidManifest.xml:9:19
intent-filter#android.intent.action.MAIN+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:11:11
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:12:17
	android:name
		ADDED from AndroidManifest.xml:12:25
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:13:17
	android:name
		ADDED from AndroidManifest.xml:13:27
activity#joanne.cs250.SemesterList
ADDED from AndroidManifest.xml:17:9
	android:label
		ADDED from AndroidManifest.xml:18:10
	android:name
		ADDED from AndroidManifest.xml:17:19
activity#joanne.cs250.SemesterCourses
ADDED from AndroidManifest.xml:21:6
	android:label
		ADDED from AndroidManifest.xml:22:10
	android:name
		ADDED from AndroidManifest.xml:21:16
activity#joanne.cs250.CourseView
ADDED from AndroidManifest.xml:25:6
	android:label
		ADDED from AndroidManifest.xml:26:10
	android:name
		ADDED from AndroidManifest.xml:25:16
activity#joanne.cs250.QuickCalc
ADDED from AndroidManifest.xml:29:6
	android:label
		ADDED from AndroidManifest.xml:30:10
	android:name
		ADDED from AndroidManifest.xml:29:16
activity#joanne.cs250.MyPreferences
ADDED from AndroidManifest.xml:33:6
	android:label
		ADDED from AndroidManifest.xml:34:2
	android:name
		ADDED from AndroidManifest.xml:33:16
intent-filter#joanne.cs250.ACTION_USER_PREFERENCE
ADDED from AndroidManifest.xml:35:2
action#joanne.cs250.ACTION_USER_PREFERENCE
ADDED from AndroidManifest.xml:36:2
	android:name
		ADDED from AndroidManifest.xml:36:10
