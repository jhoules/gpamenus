package joanne.cs250;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class MyPreferences extends PreferenceActivity {

	public void onCreate(Bundle myBundle) {
		super.onCreate(myBundle);
		addPreferencesFromResource(R.xml.prefs);
	}

}
